import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AccessibilityFilterOptionsEnum,
  AssignmentFilterOptionsEnum,
  AssignmentFiltersEnum
} from '../../../../core/enums/assign-filters.enum';
import { RecordTypeEnum } from '../../../../core/enums/record-type.enum';
import {
  IAssignmentFilter,
  IAssignmentFilterOption,
  IAssignmentFilterRequestData,
  IOpportunityFilterRequestData
} from '../../../../core/interfaces/assign-filter.interface';
import { RoadmapService } from '../../../../core/services/roadmap.service';

@Component({
  selector: 'app-assign-filters',
  templateUrl: './assign-filters.component.html',
  styleUrls: ['./assign-filters.component.scss']
})
export class AssignFiltersComponent implements OnInit {

  private filterRequestData = {} as IAssignmentFilterRequestData | IOpportunityFilterRequestData;
  private opportunityFilters: IAssignmentFilter[] = this.createDefaultFilters();
  private solutionFilters: IAssignmentFilter[] = this.createDefaultFilters();

  @Input() filtersType: RecordTypeEnum = RecordTypeEnum.Opportunity;
  @Input() page = 1;
  @Input() assigned: number[] = [];
  @Output() filtersChange = new EventEmitter<boolean>();

  constructor(private roadmapService: RoadmapService,
              private route: ActivatedRoute
  ) {}

  get filters(): IAssignmentFilter[] {
    switch (this.filtersType) {
      case RecordTypeEnum.Opportunity: return this.opportunityFilters;
      case RecordTypeEnum.Solution: return this.solutionFilters;
    }
  }

  ngOnInit() {
    this.filterRequestData.mapId = +this.route.snapshot.paramMap.get('mapId') || 1566;
    this.filterRequestData.page = this.page;
    this.filterRequestData.assigned = this.assigned;

    this.roadmapService.getOpportunityTypes().subscribe(types => {
      this.opportunityFilters.unshift({
        name: 'Filter by type',
        type: AssignmentFiltersEnum.OpportunityTypes,
        isAllSelected: false,
        isAnySelected: false,
        options: types.map(type => ({ id: type.id, name: type.name, selected: false }))
      });
    });
  }

  selectOption(filter: IAssignmentFilter, option: IAssignmentFilterOption) {
    option.selected = !option.selected;
    filter.isAnySelected = filter.options.some(opt => opt.selected);
    filter.isAllSelected = filter.options.every(opt => opt.selected);
    this.filterAssignments();
  }

  selectAllOptions(filter: IAssignmentFilter) {
    if (filter.isAllSelected) {
      filter.isAllSelected = false;
      filter.isAnySelected = false;
      filter.options.forEach(opt => opt.selected = false);

    } else {
      filter.isAllSelected = true;
      filter.isAnySelected = true;
      filter.options.forEach(opt => opt.selected = true);
    }
    this.filterAssignments();
  }

  private filterAssignments() {

    for (const filter of this.filters) {

      switch (filter.type) {
        case AssignmentFiltersEnum.OpportunityTypes: {
          const { opportunityTypes, ...rest } = this.filterRequestData as IOpportunityFilterRequestData;
          const ids = filter.options.filter(opt => opt.selected).map(opt => opt.id) as number[];

          this.filterRequestData = {
            ...rest,
            ...(ids.length > 0 ? { opportunityTypes: ids } : {})
          };
          break;
        }

        case AssignmentFiltersEnum.Assignment: {
          const { assignment, ...rest } = this.filterRequestData as IAssignmentFilterRequestData;

          if (filter.isAnySelected === false) {
            this.filterRequestData = rest;

          } else if (filter.isAllSelected) {
            this.filterRequestData.assignment = AssignmentFilterOptionsEnum.All;

          } else {
            this.filterRequestData.assignment = filter.options.find(opt => opt.selected).id as AssignmentFilterOptionsEnum;
          }
          break;
        }

        case AssignmentFiltersEnum.Accessibility: {
          const { accessibility, ...rest } = this.filterRequestData as IAssignmentFilterRequestData;

          if (filter.isAnySelected === false) {
            this.filterRequestData = rest;

          } else if (filter.isAllSelected) {
            this.filterRequestData.accessibility = null;

          } else {
            this.filterRequestData.accessibility = filter.options.find(opt => opt.selected).id as AccessibilityFilterOptionsEnum;
          }
          break;
        }
      }
    }

    this.roadmapService.filterAssignments(this.filtersType, this.filterRequestData)
      .subscribe(
        () => this.filtersChange.emit(true),
        () => this.filtersChange.emit(false)
      );
  }

  private createDefaultFilters(): IAssignmentFilter[] {

    return [
      {
        name: 'Filter by assigned',
        type: AssignmentFiltersEnum.Assignment,
        isAllSelected: true,
        isAnySelected: true,
        options: [
          {
            id: AssignmentFilterOptionsEnum.Assigned,
            name: 'Assigned',
            selected: true
          },
          {
            id: AssignmentFilterOptionsEnum.NotAssigned,
            name: 'Not assigned',
            selected: true
          }
        ]
      },
      {
        name: 'Filter by access',
        type: AssignmentFiltersEnum.Accessibility,
        isAllSelected: true,
        isAnySelected: true,
        options: [
          {
            id: AccessibilityFilterOptionsEnum.Public,
            name: 'Public',
            selected: true
          },
          {
            id: AccessibilityFilterOptionsEnum.Private,
            name: 'Private',
            selected: true
          }
        ]
      }
    ];
  }
}
