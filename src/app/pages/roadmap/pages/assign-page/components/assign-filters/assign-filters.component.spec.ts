import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignFiltersComponent } from './assign-filters.component';

describe('AssignFiltersComponent', () => {
  let component: AssignFiltersComponent;
  let fixture: ComponentFixture<AssignFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
