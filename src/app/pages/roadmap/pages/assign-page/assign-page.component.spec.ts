import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignPageComponent } from './assign-page.component';

describe('AssignPageComponent', () => {
  let component: AssignPageComponent;
  let fixture: ComponentFixture<AssignPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
