import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssignPageComponent } from './assign-page.component';

const routes: Routes = [{ path: '', component: AssignPageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignPageRoutingModule { }
