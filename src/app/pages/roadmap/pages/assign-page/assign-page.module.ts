import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssignPageRoutingModule } from './assign-page-routing.module';
import { AssignPageComponent } from './assign-page.component';
import { AssignFiltersComponent } from './components/assign-filters/assign-filters.component';


@NgModule({
  declarations: [AssignPageComponent, AssignFiltersComponent],
  imports: [
    CommonModule,
    AssignPageRoutingModule
  ]
})
export class AssignPageModule { }
