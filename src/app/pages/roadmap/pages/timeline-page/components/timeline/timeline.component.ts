import { CdkDragDrop } from '@angular/cdk/drag-drop';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  Renderer2,
  ViewChild,
  ViewChildren
} from '@angular/core';
import * as moment from 'moment';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';
import { RecordTypeEnum } from '../../../../core/enums/record-type.enum';
import { SelectionTypeEnum } from '../../../../core/enums/selection-type.enum';
import { ISelectionResult } from '../../../../core/interfaces/multiselect.interface';
import { ITimelineRecordDeleteData } from '../../../../core/interfaces/timeline-update.interface';
import { IActiveWeek, ITimelineRecord } from '../../../../core/interfaces/timeline.interface';
import { TimelineModel } from '../../../../core/models/timeline.model';
import { TimelineService } from '../../services/timeline.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineComponent implements OnInit, AfterViewInit, OnDestroy {

  private ngUnsubscribe = new Subject();
  private initialTimelineRecords: ITimelineRecord[];

  recordTypes = RecordTypeEnum;
  weekWidth: number;
  activeYearIndex = 0;

  @Input() selectedFilters$: Observable<ISelectionResult[]>;
  @Input() searchedRecords$: Observable<ITimelineRecord[]>;
  @Input() timeline: TimelineModel;

  @ViewChild('weeksRef', { static: false }) weeksRef: ElementRef<HTMLDivElement>;
  @ViewChildren('yearsRef') yearsRef: QueryList<ElementRef<HTMLDivElement>>;

  constructor(private renderer: Renderer2,
              private changeDetectorRef: ChangeDetectorRef,
              private timelineService: TimelineService
  ) {}

  @HostListener('window:resize')
  onWindowResize() {
    this.calculateWeekWidth();
  }

  ngOnInit() {
    this.initialTimelineRecords = [...this.timeline.records];
    this.setupTimeline();
    this.sortTimeline();

    this.timelineService.recordChange$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(changed => {
      const record = this.timeline.records.find(rec => rec.id === changed.id);
      const recordIndex = this.timeline.records.indexOf(record);

      if ((changed as ITimelineRecordDeleteData).isDelete) {
        this.timeline.records.splice(recordIndex, 1);
      } else {
        Object.keys(record).forEach(prop => record[prop] = changed[prop] || record[prop]);
        this.createActiveWeeks(record);
      }

      this.sortTimeline();
    });

    combineLatest(
      this.selectedFilters$.pipe(map(selections => this.filterRecords(selections))),
      this.searchedRecords$.pipe(startWith(this.timeline.records))
    )
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(([filteredRecords, searchedRecords]) => {
        this.timeline.records = filteredRecords.filter(record => searchedRecords.indexOf(record) > -1);
        this.sortTimeline();
        this.changeDetectorRef.detectChanges();
      });
  }

  ngAfterViewInit() {
    this.calculateWeekWidth();
    // Detect changes after tooltips created
    setTimeout(() => this.changeDetectorRef.detectChanges(), 100);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  isArray(arr) {
    return Array.isArray(arr);
  }

  scrollToYear(index: number) {
    this.yearsRef.toArray()[index].nativeElement.scrollIntoView({ behavior: 'smooth' });
    this.activeYearIndex = index;
  }

  onRecordDrop(event: CdkDragDrop<HTMLDivElement>, record: ITimelineRecord) {
    const { weeks, ...changedRecord } = record;
    const recordClone = { ...changedRecord };
    changedRecord.startDate = this.timeline.weeks[event.currentIndex];

    const phases = [...this.timeline.phases];
    if (event.distance.x < 0 && this.isOverlapDate(changedRecord.startDate)) {
      phases.reverse();
    }
    const mom = moment(changedRecord.startDate);
    const phase = phases.find(p => mom.isBetween(p.startDate, p.endDate, 'day', '[)'));
    changedRecord.phaseId = phase.id;

    this.timelineService.applyRecord(changedRecord);
    this.timelineService.updateRecord(changedRecord).subscribe(
      { error: () => this.timelineService.applyRecord(recordClone) }
    );
  }

  selectRecord(record: ITimelineRecord) {
    this.timelineService.selectRecord(record);
    this.timelineService.toggleRecordSetup(true);
  }

  private setupTimeline() {
    this.timeline.records.forEach((record: ITimelineRecord) => this.createActiveWeeks(record));
  }

  private createActiveWeeks(record: ITimelineRecord) {
    const weekStartDate = moment(record.startDate);
    let activeWeeks: IActiveWeek[];
    record.weeks = [...this.timeline.weeks];

    if (record.type === RecordTypeEnum.Opportunity) {
      activeWeeks = new Array(record.duration).fill(null).map(() => ({ type: 'opportunity' }));

    } else if (record.type === RecordTypeEnum.Solution) {
      const preparePart = new Array(record.prepare).fill(null).map(() => ({ type: 'prepare' }));
      const buildPart = new Array(record.build).fill(null).map(() => ({ type: 'build' }));
      const deployPart = new Array(record.deploy).fill(null).map(() => ({ type: 'deploy' }));
      activeWeeks = preparePart.concat(buildPart, deployPart);
    }

    for (let i = 0; i < activeWeeks.length; ++i) {
      const activeWeek = activeWeeks[i];
      activeWeek.date = weekStartDate.toDate();

      while (this.isGapWeek(activeWeek.date)) {
        activeWeeks.splice(i, 0, { type: 'gap', date: activeWeek.date });
        activeWeek.date = weekStartDate.add(1, 'week').toDate();
        i++;
      }

      weekStartDate.add(1, 'week');
    }

    const replaceIndex = record.weeks.findIndex(week => moment(week as Date).isSame(record.startDate, 'week'));
    record.weeks.splice(replaceIndex, activeWeeks.length, activeWeeks);
    this.changeDetectorRef.detectChanges();
  }

  private isGapWeek(week: Date): boolean {
    const weekMoment = moment(week);
    const isGap = !this.timeline.phases.some(
      phase => weekMoment.isBetween(phase.startDate, phase.endDate, 'day', '[)')
    );
    const lastPhase = this.timeline.phases[this.timeline.phases.length - 1];
    const isTimelineEnd = lastPhase ? weekMoment.isAfter(lastPhase.endDate) : true;

    return isGap && !isTimelineEnd;
  }

  private isOverlapDate(date: Date): boolean {
    const weekMoment = moment(date);
    let count = 0;

    for (const phase of this.timeline.phases) {
      if (weekMoment.isBetween(phase.startDate, phase.endDate, 'day', '[)')) {
        count++;
      }
    }
    return count > 1;
  }

  private sortTimeline() {
    this.timeline.records.sort((a, b) => {
      return a.startDate.getTime() - b.startDate.getTime();
    });
    this.changeDetectorRef.detectChanges();
  }

  private calculateWeekWidth() {
    this.weekWidth = this.weeksRef.nativeElement.offsetWidth / this.timeline.weeks.length;
    this.changeDetectorRef.detectChanges();
  }

  private filterRecords(selections: ISelectionResult[]): ITimelineRecord[] {
    let records = [...this.initialTimelineRecords];

    for (const selectedFilter of selections) {
      const options = selectedFilter.selectedOptions;
      let filterFn: (record: ITimelineRecord) => boolean;

      switch (selectedFilter.selectionType) {
        case SelectionTypeEnum.Teams:
          filterFn = record => options.length === 0 || options.some(id => record.teamsId.some(teamId => teamId === id));
          break;
        case SelectionTypeEnum.Phases:
          filterFn = record => options.length === 0 || options.some(id => id === record.phaseId);
          break;
        case SelectionTypeEnum.UserDefined:
          filterFn = record => options.length === 0 || options.some(id => record.userDefinedId.some(uDefId => uDefId === id));
          break;
      }

      records = records.filter(filterFn);
    }

    return records;
  }
}
