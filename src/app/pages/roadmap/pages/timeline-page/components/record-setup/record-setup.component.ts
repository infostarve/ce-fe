import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RecordTypeEnum } from '../../../../core/enums/record-type.enum';
import { ITimelineRecord } from '../../../../core/interfaces/timeline.interface';
import { TimelineModel } from '../../../../core/models/timeline.model';
import { TimelineService } from '../../services/timeline.service';
import * as moment from 'moment';

@Component({
  selector: 'app-record-setup',
  templateUrl: './record-setup.component.html',
  styleUrls: ['./record-setup.component.scss']
})
export class RecordSetupComponent implements OnInit, OnDestroy {

  private ngUnsubscribe = new Subject();

  recordAllTypes = RecordTypeEnum;
  updatingRecord: Partial<ITimelineRecord>;
  recordForm: FormGroup;

  isDeleteModalShown = false;
  isStartDateChanged = false;
  isPhaseIdChanged = false;

  @Input() timeline: TimelineModel;

  constructor(private formBuilder: FormBuilder,
              private timelineService: TimelineService
  ) {}

  get isShown(): boolean {
    return this.timelineService.isRecordSetupShown;
  }

  close() {
    this.timelineService.toggleRecordSetup(false);
  }

  ngOnInit() {
    this.updatingRecord = {
      teamsId: [],
      userDefinedId: [],
      phaseId: null,
      startDate: new Date(),
      prepare: 0,
      build: 0,
      deploy: 0,
      duration: 0
    };
    this.recordForm = this.formBuilder.group(this.updatingRecord);

    this.timelineService.recordSelect$.pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((record: ITimelineRecord) => {
        this.updatingRecord = record;

        this.patchRecordForm(record);

        if (record.type === RecordTypeEnum.Opportunity) {
          this.recordForm.controls.duration.enable();
          this.recordForm.controls.prepare.disable();
          this.recordForm.controls.build.disable();
          this.recordForm.controls.deploy.disable();

        } else if (record.type === RecordTypeEnum.Solution) {
          this.recordForm.controls.duration.disable();
          this.recordForm.controls.prepare.enable();
          this.recordForm.controls.build.enable();
          this.recordForm.controls.deploy.enable();
        }
      });

    this.timelineService.recordChange$
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((record: ITimelineRecord) => this.patchRecordForm(record));

    this.recordForm.controls.phaseId.valueChanges.subscribe(id => {
      const phase = this.timeline.phases.find(p => p.id === id);
      this.recordForm.controls.startDate.setValue(phase.startDate, { emitEvent: false });
      this.isPhaseIdChanged = this.updatingRecord.phaseId !== id;
    });

    this.recordForm.controls.startDate.valueChanges.subscribe(date => {
      const dateMoment = moment(date);
      const phase = this.timeline.phases.find(p => dateMoment.isBetween(p.startDate, p.endDate, 'week', '[)'));
      this.recordForm.controls.phaseId.setValue(phase.id, { emitEvent: false });
      this.isStartDateChanged = !moment(this.updatingRecord.startDate).isSame(date);
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  applyRecordChanges() {
    const changedRecord: ITimelineRecord = {
      ...this.recordForm.value,
      id: this.updatingRecord.id
    };

    this.timelineService.updateRecord(changedRecord).subscribe(() => {
      this.timelineService.applyRecord(changedRecord);
      this.timelineService.toggleRecordSetup(false);
    });
  }

  deleteRecord(id: number, type: RecordTypeEnum) {
    const deleteRecord = { id, type };

    this.timelineService.deleteRecord(deleteRecord).subscribe(() => {
      this.timelineService.applyRecord({ ...deleteRecord, isDelete: true });
      this.timelineService.toggleRecordSetup(false);
    });
  }

  private patchRecordForm(recordData: ITimelineRecord) {
    this.recordForm.patchValue({
      teamsId: recordData.teamsId,
      userDefinedId: recordData.userDefinedId,
      phaseId: recordData.phaseId,
      startDate: recordData.startDate,
      prepare: recordData.prepare,
      build: recordData.build,
      deploy: recordData.deploy,
      duration: recordData.duration
    });
  }
}
