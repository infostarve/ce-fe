import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ITimelineRecord } from '../../../../core/interfaces/timeline.interface';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {

  private previousQuery = '';
  private recordsCopy: ITimelineRecord[];

  @Input() records: ITimelineRecord[];
  @Output() recordsSearch = new EventEmitter<ITimelineRecord[]>();

  ngOnInit() {
    this.recordsCopy = [...this.records];
  }

  search(query: string) {
    if (query.length > 2 || this.previousQuery.length > query.length) {
      const foundRecords = this.recordsCopy.filter(record => record.title.toLowerCase().includes(query.toLowerCase()));
      this.recordsSearch.emit(foundRecords);
      this.previousQuery = query;
    }
  }
}
