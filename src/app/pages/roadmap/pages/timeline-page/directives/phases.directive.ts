import { AfterViewInit, Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import * as moment from 'moment';
import { TimelineModel } from '../../../core/models/timeline.model';

@Directive({
  selector: '[appPhases]'
})
export class PhasesDirective implements AfterViewInit {

  private hostElement: HTMLDivElement;
  private phaseElements: HTMLDivElement[];

  @Input() timeline: TimelineModel;

  constructor(private hostRef: ElementRef<HTMLDivElement>,
              private renderer: Renderer2
  ) {}

  @HostListener('window:resize')
  onResize() {
    this.calculatePhaseStyles();
  }

  ngAfterViewInit() {
    this.hostElement = this.hostRef.nativeElement;
    this.phaseElements = Array.from(this.hostElement.children) as HTMLDivElement[];
    this.calculatePhaseStyles();
  }

  private calculatePhaseStyles() {
    const pixelsPerWeek = this.hostElement.offsetWidth / this.timeline.weeks.length;
    let lastPhaseEndDate;

    this.phaseElements.forEach((phaseElement, index) => {
      const phase = this.timeline.phases[index];
      const phaseStartDiff = moment(this.timeline.years[0]).diff(phase.startDate);
      const phaseStartDuration = moment.duration(phaseStartDiff);
      const phaseStartDurationInWeeks = Math.abs(Math.round(phaseStartDuration.asWeeks()));
      const phaseStartOffset = phaseStartDurationInWeeks * pixelsPerWeek;

      const phaseEndDiff = moment(phase.startDate).diff(phase.endDate);
      const phaseEndDuration = moment.duration(phaseEndDiff);
      const phaseEndDurationInWeeks = Math.abs(Math.round(phaseEndDuration.asWeeks()));
      const phaseWidth = phaseEndDurationInWeeks * pixelsPerWeek;

      this.renderer.setStyle(phaseElement, 'transform', `translateX(${phaseStartOffset}px)`);
      this.renderer.setStyle(phaseElement, 'width', phaseWidth + 'px');

      const createGapOrOverlap = (type: 'gap' | 'overlap') => {
        const gapDiff = moment(lastPhaseEndDate).diff(phase.startDate);
        const gapDuration = moment.duration(gapDiff);
        const gapDurationInWeeks = Math.abs(Math.round(gapDuration.asWeeks())) + (type === 'overlap' ? 1 : 0);
        const gapWidth = gapDurationInWeeks * pixelsPerWeek;

        let gapElement: HTMLDivElement;
        if (phaseElement.previousElementSibling.classList.contains(type)) {
          gapElement = phaseElement.previousElementSibling as HTMLDivElement;
        } else {
          gapElement = this.renderer.createElement('div');
          const leftBorderColor = this.timeline.phases[index - 1].color;
          const rightBorderColor = phase.color;

          this.renderer.setStyle(gapElement, 'borderLeftColor', type === 'overlap' ? rightBorderColor : leftBorderColor);
          this.renderer.setStyle(gapElement, 'borderRightColor', type === 'overlap' ? leftBorderColor : rightBorderColor);
          this.renderer.addClass(gapElement, type);
          this.renderer.insertBefore(this.hostElement, gapElement, phaseElement);
        }

        const translateX = type === 'overlap' ? phaseStartOffset : phaseStartOffset - gapWidth;
        this.renderer.setStyle(gapElement, 'width', gapWidth + 'px');
        this.renderer.setStyle(gapElement, 'transform', `translateX(${translateX}px)`);
      };

      if (lastPhaseEndDate && moment(lastPhaseEndDate).isBefore(phase.startDate)) {
        createGapOrOverlap('gap');

      } else if (lastPhaseEndDate && moment(lastPhaseEndDate).isAfter(phase.startDate)) {
        createGapOrOverlap('overlap');
      }

      lastPhaseEndDate = phase.endDate;
    });
  }
}
