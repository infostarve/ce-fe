import { AfterViewInit, Directive, ElementRef, Input, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TimelineModel } from '../../../core/models/timeline.model';

@Directive({
  selector: '[appMouseTracker]'
})
export class MouseTrackerDirective implements AfterViewInit, OnDestroy {

  private ngUnsubscribe = new Subject();
  private tracker: HTMLDivElement = this.mouseTrackerRef.nativeElement;

  @Input() timeline: TimelineModel;

  constructor(private mouseTrackerRef: ElementRef<HTMLDivElement>) {}

  private get dayWidth(): number {
    return this.tracker.parentElement.parentElement.offsetWidth / this.timeline.weeks.length / 7;
  }

  ngAfterViewInit() {
    const tracker = this.mouseTrackerRef.nativeElement;

    fromEvent(tracker.parentElement.parentElement, 'mousemove').pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((event: MouseEvent) => {
        const trackContainer = event.currentTarget as HTMLDivElement;
        const posX = event.clientX + trackContainer.scrollLeft - trackContainer.getBoundingClientRect().left;
        const daysCount = posX / this.dayWidth;
        const timelineStartDate = this.timeline.weeks[0];
        tracker.firstElementChild.textContent = moment(timelineStartDate).add(daysCount, 'days').format('MMM D');
        tracker.style.transform = `translateX(${posX - this.tracker.offsetWidth / 2}px)`;
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
