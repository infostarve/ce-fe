import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MultiselectSidebarModule } from '../../features/multiselect-sidebar/multiselect-sidebar.module';
import { RecordSetupComponent } from './components/record-setup/record-setup.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { MouseTrackerDirective } from './directives/mouse-tracker.directive';
import { PhasesDirective } from './directives/phases.directive';
import { TimelineService } from './services/timeline.service';
import { TimelinePageRoutingModule } from './timeline-page-routing.module';
import { TimelinePageComponent } from './timeline-page.component';

@NgModule({
  declarations: [
    TimelinePageComponent,
    TimelineComponent,
    PhasesDirective,
    MouseTrackerDirective,
    SearchbarComponent,
    RecordSetupComponent,
    SearchbarComponent,
  ],
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    ReactiveFormsModule,
    MultiselectSidebarModule,
    TimelinePageRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
  ],
  providers: [
    TimelineService
  ]
})
export class TimelinePageModule {}
