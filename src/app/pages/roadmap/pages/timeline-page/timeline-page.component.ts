import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, ReplaySubject } from 'rxjs';
import { ISelectionResult } from '../../core/interfaces/multiselect.interface';
import { ITimelineRecord } from '../../core/interfaces/timeline.interface';
import { TimelineModel } from '../../core/models/timeline.model';
import { TimelineService } from './services/timeline.service';

@Component({
  selector: 'app-timeline-page',
  templateUrl: './timeline-page.component.html',
  styleUrls: ['./timeline-page.component.scss']
})
export class TimelinePageComponent implements OnInit {
  deleteNewPop = false;
  gotoRoadmapPop = false;
  gotoAssignPop = false;
  cancelNewPop = false;
  cancel1NewPop = false;

  filteredDataSubject$ = new ReplaySubject<ISelectionResult[]>(1);
  filteredData$ = this.filteredDataSubject$.asObservable();

  searchedDataSubject$ = new ReplaySubject<ITimelineRecord[]>(1);
  searchedData$ = this.searchedDataSubject$.asObservable();

  timeline$: Observable<TimelineModel>;

  constructor(private timelineService: TimelineService,
              private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const mapId = this.route.snapshot.paramMap.get('mapId');
    this.timeline$ = this.timelineService.getTimelineModel(mapId);
  }
}
