import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { ITimelineRecordDeleteData } from '../../../core/interfaces/timeline-update.interface';
import { ITimelineRecord } from '../../../core/interfaces/timeline.interface';
import { TimelineModel } from '../../../core/models/timeline.model';
import { mockedBackendData } from './mock-data';

@Injectable()
export class TimelineService {

  private showRecordSetup = false;
  private recordSelectSubject$ = new Subject<ITimelineRecord>();
  private recordChangeSubject$ = new Subject<ITimelineRecord | ITimelineRecordDeleteData>();

  recordSelect$ = this.recordSelectSubject$.asObservable();
  recordChange$ = this.recordChangeSubject$.asObservable();

  constructor(private http: HttpClient) {}

  get isRecordSetupShown() {
    return this.showRecordSetup;
  }

  toggleRecordSetup(show: boolean) {
    this.showRecordSetup = show;
  }

  selectRecord(record: ITimelineRecord) {
    this.recordSelectSubject$.next(record);
  }

  getTimelineModel(mapId: string): Observable<TimelineModel> {
    // return this.http.get<ITimelineResponse>(`/api/map/${mapId}/roadmap/timeline`).pipe(
    //   map(response => new TimelineModel(response))
    // );
    return of(new TimelineModel(mockedBackendData as any));
  }

  applyRecord(updatedRecord: ITimelineRecord | ITimelineRecordDeleteData) {
    this.recordChangeSubject$.next(updatedRecord);
  }

  updateRecord(changedRecord: ITimelineRecord): Observable<any> {
    return of('');
    // let endpointPath = '/api/map/roadmap/';
    //
    // if (changedRecord.type === RecordTypeEnum.Opportunity) {
    //   endpointPath += 'opportunity';
    // } else if (changedRecord.type === RecordTypeEnum.Solution) {
    //   endpointPath += 'solution';
    // }
    //
    // return this.http.put(endpointPath, changedRecord);
  }

  deleteRecord(deleteRecord: ITimelineRecordDeleteData): Observable<any> {
    return of('');
    // const options = {
    //   headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    //   body: deleteRecord
    // };
    //
    // return this.http.delete('/api/map/roadmap/timeline', options);
  }
}
