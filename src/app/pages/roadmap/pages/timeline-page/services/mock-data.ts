export const mockedBackendData = {
  "opportunities":[
    {
      "opportunityId":25002,
      "duration":8,
      "id":18,
      "title":"Enterprise Reporting Automation and Centralization",
      "type":0,
      "startDate":"2020-03-15T00:00:00",
      "phaseId":21,
      "teamsId":[
        1,
        2,
        3
      ],
      "userDefinedId":[
        17
      ]
    },
    {
      "opportunityId":25034,
      "duration":5,
      "id":19,
      "title":"Sales Force Performance Improvement",
      "type":0,
      "startDate":"2020-03-15T00:00:00",
      "phaseId":21,
      "teamsId":[
        1,
        2,
        3
      ],
      "userDefinedId":[
        17
      ]
    },
    {
      "opportunityId":25085,
      "duration":12,
      "id":22,
      "title":"Personalized Programmatic Messaging",
      "type":0,
      "startDate":"2020-03-15T00:00:00",
      "phaseId":5,
      "teamsId":[
        1,
        2
      ],
      "userDefinedId":[
        17
      ]
    },
    {
      "opportunityId":25085,
      "duration":3,
      "id":23,
      "title":"Personalized Programmatic Messaging",
      "type":0,
      "startDate":"2020-03-17T00:00:00",
      "phaseId":6,
      "teamsId":[
        1,
        2
      ],
      "userDefinedId":[
        17
      ]
    }
  ],
  "solutions":[
    {
      "opportunityId":25032,
      "prepare":3,
      "build": 6,
      "deploy": 4,
      "id":29,
      "title":"Solution is very great",
      "type":1,
      "startDate":"2020-05-17T00:00:00",
      "phaseId":6,
      "teamsId":[
        1,
        2
      ],
      "userDefinedId":[
        17
      ]
    },
    {
      "opportunityId":25132,
      "prepare":3,
      "build": 6,
      "deploy": 4,
      "id":25,
      "title":"Solution 2021 is very great",
      "type":1,
      "startDate":"2021-05-17T00:00:00",
      "phaseId":21,
      "teamsId":[
        1,
        2
      ],
      "userDefinedId":[
        17
      ]
    },
    {
      "opportunityId":25232,
      "prepare":6,
      "build": 8,
      "deploy": 4,
      "id":26,
      "title":"Solution 2022 is very great",
      "type":1,
      "startDate":"2025-05-17T00:00:00",
      "phaseId":21,
      "teamsId":[
        1,
        2,
        3
      ],
      "userDefinedId":[
        17
      ]
    }
  ],
  "filters":{
    "teams":[
      {
        "id":1,
        "name":"Super Team #1",
        "roadmapId":1
      },
      {
        "id":2,
        "name":"Team #2",
        "roadmapId":1
      },
      {
        "id":3,
        "name":"Team #3",
        "roadmapId":1
      }
    ],
    "phases":[
      {
        id: 5,
        name: 'Conception & Initiation phase',
        startDate: new Date('2020-01-01T00:00:00'),
        endDate: new Date('2020-05-01T00:00:00')
      },
      {
        id: 6,
        name: 'Development phase',
        startDate: new Date('2020-03-01T00:00:00'),
        endDate: new Date('2020-06-08T00:00:00')
      },
      {
        id: 21,
        name: 'Build & Deploy phase',
        startDate: new Date('2020-07-01T00:00:00'),
        endDate: new Date('2020-12-01T00:00:00')
      }
    ],
    "userDefinedFilter":{
      "options":[
        {
          "id":17,
          "name":"Workstream #17",
          "roadmapId":1
        }
      ],
      "id":14,
      "name":"Project workstreams",
      "roadmapId":1
    }
  }
};
