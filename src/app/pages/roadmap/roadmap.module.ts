import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDatepickerModule, MatInputModule, MatNativeDateModule } from '@angular/material';
// import { OpportunityTileComponent } from '@app/mapslist/components/opportunity-tile/opportunity-tile.component';
// import { RoadmapAssignmentComponent } from './containers/roadmap-assignment/roadmap-assignment.component';
// import { RoadmapmanageComponent } from './containers/roadmapmanage/roadmapmanage.component';
// TO DO: Avoid importing an entire module and instead get tonly the tiles component injected
// import { RoadmapFooterComponent } from './features/roadmap-footer/roadmap-footer.component';

import { RoadmapRoutingModule } from './roadmap-routing.module';


@NgModule({
  declarations: [
    // RoadmapmanageComponent,
    // RoadmapAssignmentComponent,
    // RoadmapFooterComponent
  ],
  imports: [
    CommonModule,
    RoadmapRoutingModule,
    MatInputModule,
    MatDatepickerModule,
    FormsModule,
    MatNativeDateModule
    // BsDropdownModule.forRoot(),
    // MapslistModule,
    // SharedModule,
    // MultiselectSidebarModule
  ]
})
export class RoadmapModule {}
