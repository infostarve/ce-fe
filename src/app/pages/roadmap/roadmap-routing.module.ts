import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { RoadmapAssignmentComponent } from './containers/roadmap-assignment/roadmap-assignment.component';
// import { RoadmapmanageComponent } from './containers/roadmapmanage/roadmapmanage.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'timeline'
  },
  // {
  //   path: 'manage',
  //   component: RoadmapmanageComponent, data: { pageName: 'Roadmap - Manage Teams, Phases, others' }
  // },
  // {
  //   path: 'assign',
  //   component: RoadmapAssignmentComponent, data: { pageName: 'Roadmap - Assign Opportunities Solutions' }
  // },
  {
    path: 'timeline',
    loadChildren: () => import('./pages/timeline-page/timeline-page.module').then(m => m.TimelinePageModule),
    data: { pageName: 'Roadmap Timeline' }
  },
  { path: 'assign', loadChildren: () => import('./pages/assign-page/assign-page.module').then(m => m.AssignPageModule) }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoadmapRoutingModule { }
