import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { RecordTypeEnum } from '../enums/record-type.enum';
import { IAssignmentFilterRequestData, IOpportunityFilterRequestData } from '../interfaces/assign-filter.interface';
import { IOpportunityType } from '../interfaces/opportunity-type.interface';

@Injectable({
  providedIn: 'root'
})
export class RoadmapService {

  constructor(private http: HttpClient) {}

  getOpportunityTypes(): Observable<IOpportunityType[]> {
    // return this.http.get('api/opportunitytype/GetOpportunityTypeList').pipe(
    //   map((response: any) => {
    //     return response.Items.map(item => ({ id: item.Id, name: item.Name }));
    //   })
    // );

    return of([{ id: 5, name: 'Cloud' }, { id: 2, name: 'Cognitive Engagement' }]);
  }

  filterAssignments(type: RecordTypeEnum, filterData: IAssignmentFilterRequestData | IOpportunityFilterRequestData): Observable<any> {
    let entityPath: string;
    switch (type) {
      case RecordTypeEnum.Opportunity: entityPath = 'opportunities'; break;
      case RecordTypeEnum.Solution: entityPath = 'solutions'; break;
    }

    return this.http.post(`https://cetestingapp.azurewebsites.net/api/map/${entityPath}/filter`, filterData);
  }
}
