export class Roadmapconfig {

    teams: { Id: number, Name: string, isEditable: boolean, ChangeStatus: number }[] = [];

    phases: { Id: number, Name: string, isEditable: boolean, StartDate: Date, EndDate: Date, ChangeStatus: number }[] = [];
    customfield: { Id: number, Name: string, isEditable?: boolean,ChangeStatus: number,RoadmapId: number }[] = [];
    userdefinedfield = "User-Defined Section";
    UserDefinedFilter: UserDefinedConfig;
    customfilterstaus:boolean;
    customfilterid:boolean;
}
export class UserDefinedConfig {
    Name?: string;
    Id: number;
    ChangeStatus: number;
    RoadmapId: number;
    Options: Array<userDefinedOption>;
}

export class userDefinedOption {
    Name?: string;
    Id: number;
    ChangeStatus: number;
    RoadmapId: number;
} 