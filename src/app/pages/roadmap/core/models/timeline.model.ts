import * as moment from 'moment';
import { Moment } from 'moment';
import { ITimelineResponse } from '../interfaces/timeline-response.interface';
import { ITimeline, ITimelinePhase, ITimelineRecord } from '../interfaces/timeline.interface';
import { IRoadmapParameters } from '../interfaces/roadmap-parameters.interface';

export class TimelineModel implements ITimeline {

  private readonly timelineEndMoment: Moment;
  private readonly timelineResponse: ITimelineResponse;

  phases: ITimelinePhase[];
  records: ITimelineRecord[];
  years: Date[];
  weeks: Date[];
  months: Date[];
  filters: IRoadmapParameters;

  constructor(timelineResponse: ITimelineResponse) {
    this.timelineResponse = timelineResponse;
    this.filters = timelineResponse.filters;
    this.phases = this.createPhases();
    this.records = this.createRecords();
    this.timelineEndMoment = this.calculateTimelineEndMoment();
    this.years = this.generateTimelineUnits('year');
    this.months = this.generateTimelineUnits('month');
    this.weeks = this.generateTimelineUnits('week');
  }

  getRecordTeams(record: ITimelineRecord): string[] {
    return record.teamsId.map(id => this.filters.teams.find(team => team.id === id).name);
  }

  getRecordPhase(record: ITimelineRecord): string {
    const foundPhase = this.filters.phases.find(phase => phase.id === record.phaseId);
    return foundPhase ? foundPhase.name : 'No phase found';
  }

  getRecordUserDefinedParameterOptions(record: ITimelineRecord): string[] {
    return record.userDefinedId.map(id => this.filters.userDefinedFilter.options.find(opt => opt.id === id).name);
  }

  getRecordDuration(record: ITimelineRecord): number {
    return record.duration || record.prepare + record.build + record.deploy;
  }

  getRecordEndDate(record: ITimelineRecord): Date {
    const duration = this.getRecordDuration(record);
    return moment(record.startDate).add(duration, 'weeks').toDate();
  }

  private createRecords(): ITimelineRecord[] {
    const { opportunities, solutions } = this.timelineResponse;
    const records = opportunities.concat(solutions as ITimelineRecord[]) as ITimelineRecord[];

    for (const record of records) {
      record.startDate = moment(record.startDate).toDate();
    }
    return records;
  }

  private createPhases(): ITimelinePhase[] {

    return this.timelineResponse.filters.phases.map<ITimelinePhase>(filter => {
      const phase = filter as ITimelinePhase;
      phase.color = `rgb(160, 220, ${Math.floor(Math.random() * 255)})`;
      return phase;
    });
  }

  private calculateTimelineEndMoment(): Moment {
    const longestRecord = this.records.reduce(
      (longest, cur) => {
        const endOfLongestMoment = moment(longest.startDate).add(this.getRecordDuration(longest));
        const endOfCurrentMoment = moment(cur.startDate).add(this.getRecordDuration(cur));
        return endOfLongestMoment.isAfter(endOfCurrentMoment) ? longest : cur;
      }
    );
    const lastPhase = this.phases[this.phases.length - 1];
    const endOfLongestRecord = moment(longestRecord.startDate).add(this.getRecordDuration(longestRecord), 'weeks');
    const endOfLastPhase = lastPhase ? moment(lastPhase.endDate) : moment().startOf('year');
    const endOfTimeline = endOfLastPhase.isAfter(endOfLongestRecord) ? endOfLastPhase : endOfLongestRecord;
    return endOfTimeline.endOf('year');
  }

  private generateTimelineUnits(unit: 'year' | 'month' | 'week'): Date[] {
    const currentDate = moment(new Date()).startOf('year');
    const dates = [];

    while (currentDate.isBefore(this.timelineEndMoment)) {
      dates.push(currentDate.toDate());
      currentDate.add(1, unit);
    }

    return dates;
  }
}
