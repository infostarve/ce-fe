export interface IRoadmapParameters {
  teams: IRoadmapParameter[];
  phases: IRoadmapPhaseParameter[];
  userDefinedFilter: IRoadmapCustomParameter;
}

export interface IRoadmapParameter {
  id: number;
  name: string;
  roadmapId: number;
}

export interface IRoadmapPhaseParameter extends IRoadmapParameter {
  startDate: Date;
  endDate: Date;
}

export interface IRoadmapCustomParameter extends IRoadmapParameter {
  options: IRoadmapParameter[];
}
