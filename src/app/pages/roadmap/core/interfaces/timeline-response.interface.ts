import { RecordTypeEnum } from '../enums/record-type.enum';
import { IRoadmapParameters } from './roadmap-parameters.interface';

export interface ITimelineResponse {
  opportunities: ITimelineOpportunity[];
  solutions: ITimelineSolution[];
  filters: IRoadmapParameters;
}

export interface ITimelineResponseRecord {
  id: number;
  title: string;
  type: RecordTypeEnum;
  startDate: Date;
  phaseId: number;
  teamsId: number[];
  userDefinedId: number[];
}

export interface ITimelineOpportunity extends ITimelineResponseRecord {
  opportunityId: string;
  duration: number;
}

export interface ITimelineSolution extends ITimelineResponseRecord {
  solutionId: number;
  prepare: number;
  build: number;
  deploy: number;
}
