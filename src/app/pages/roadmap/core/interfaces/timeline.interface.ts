import { RecordTypeEnum } from '../enums/record-type.enum';
import { ITimelineOpportunity, ITimelineSolution } from './timeline-response.interface';
import { IRoadmapParameters, IRoadmapPhaseParameter } from './roadmap-parameters.interface';

export interface ITimeline {
  phases: ITimelinePhase[];
  records: ITimelineRecord[];
  years: Date[];
  months: Date[];
  weeks: Date[];
  filters: IRoadmapParameters;
}

export interface ITimelinePhase extends IRoadmapPhaseParameter {
  color: string;
}

export interface ITimelineRecord extends ITimelineOpportunity, ITimelineSolution {
  weeks?: Array<IActiveWeek[] | Date>;
}

export interface IActiveWeek {
  type: string;
  date?: Date;
}
