export interface IOpportunityType {
  id: number;
  name: string;
}
