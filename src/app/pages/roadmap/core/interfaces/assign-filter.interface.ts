import {
  AccessibilityFilterOptionsEnum,
  AssignmentFilterOptionsEnum,
  AssignmentFiltersEnum
} from '../enums/assign-filters.enum';

export interface IAssignmentFilter {
  name: string;
  type: AssignmentFiltersEnum;
  isAllSelected: boolean;
  isAnySelected: boolean;
  options: IAssignmentFilterOption[];
}

export interface IAssignmentFilterOption {
  id: number | AssignmentFilterOptionsEnum | AccessibilityFilterOptionsEnum;
  name: string;
  selected: boolean;
}

export interface IOpportunityFilterRequestData extends IAssignmentFilterRequestData {
  opportunityTypes: number[];
}

export interface IAssignmentFilterRequestData {
  mapId: number;
  page: number;
  limit?: number;
  search?: string;
  assigned?: number[];
  accessibility?: AccessibilityFilterOptionsEnum;
  assignment?: AssignmentFilterOptionsEnum;
}
