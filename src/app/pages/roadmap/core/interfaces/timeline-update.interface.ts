import { RecordTypeEnum } from '../enums/record-type.enum';

export interface ITimelineRecordDeleteData {
  id: number;
  type: RecordTypeEnum;
  isDelete?: boolean;
}

export interface ITimelineRecordUpdateData {
  prepare?: number;
  build?: number;
  deploy?: number;
  duration: number;
  id: number;
  teams: number[];
  userDefined: number[];
  startDate: Date;
  phaseId: number;
}
