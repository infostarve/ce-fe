import { SelectionTypeEnum } from '../enums/selection-type.enum';

export interface IRoadmapMultiselect {
  name: string;
  type: SelectionTypeEnum;
  expandAllState: 'open' | 'closed';
  expandMoreState: 'open' | 'closed';
  isAllSelected: boolean;
  options: IMultiselectOption[];
}

export interface IMultiselectOption {
  id: number;
  name: string;
  selected: boolean;
}

export interface ISelectionResult {
  selectionType: SelectionTypeEnum;
  selectedOptions: number[];
}
