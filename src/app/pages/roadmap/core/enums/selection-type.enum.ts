export enum SelectionTypeEnum {
  Teams = 'teams',
  Phases = 'phases',
  UserDefined = 'userDefinedFilter'
}
