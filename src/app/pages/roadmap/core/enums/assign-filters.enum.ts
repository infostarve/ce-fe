export enum AssignmentFiltersEnum {
  OpportunityTypes,
  Assignment,
  Accessibility
}

export enum AssignmentFilterOptionsEnum {
  All = 1,
  Assigned,
  NotAssigned
}

export enum AccessibilityFilterOptionsEnum {
  Private = 'private',
  Public = 'public'
}
