export const ROADMAP_CONFIG = {
	getMapSummaryApi: 'api/map/Summary',
    activityLog: 'api/analytics/activitylog',
    getIntegerMapId: 'api/map/integerid/',
    getRole: 'api/map/menu/',
    getUserDetails: 'api/utility/',
    workshops: {
        statusPastWorkshop: 3
    },
    user: {
        // Any update to this, need to update the accessRightsRoleIds in mapLists.mapCategories
        defaultUserRoleId: 2,
        adminRoleId: 1,
        generalUserRoleId: 2,
        readOnlyUserRoleId: 3,
        externalUserRoleId: 3,
    }
	}
