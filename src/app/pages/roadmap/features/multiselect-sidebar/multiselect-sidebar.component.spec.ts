import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiselectSidebarComponent } from './multiselect-sidebar.component';

describe('MultiselectSidebarComponent', () => {
  let component: MultiselectSidebarComponent;
  let fixture: ComponentFixture<MultiselectSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiselectSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiselectSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
