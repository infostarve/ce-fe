import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SelectionTypeEnum } from '../../core/enums/selection-type.enum';
import { IMultiselectOption, IRoadmapMultiselect, ISelectionResult } from '../../core/interfaces/multiselect.interface';
import {
  IRoadmapCustomParameter,
  IRoadmapParameter,
  IRoadmapParameters
} from '../../core/interfaces/roadmap-parameters.interface';

@Component({
  selector: 'app-multiselect-sidebar',
  templateUrl: './multiselect-sidebar.component.html',
  styleUrls: ['./multiselect-sidebar.component.scss'],
  animations: [
    trigger('openClose', [
      state('open', style({ height: '*', opacity: 1 })),
      state('closed', style({ height: 0, opacity: 0 })),
      transition('closed => open', [animate('125ms ease-in-out')]),
      transition('open => closed', [animate('125ms ease-in-out')])
    ])
  ]
})
export class MultiselectSidebarComponent implements OnInit {

  private selections: ISelectionResult[];

  selectionTypes = SelectionTypeEnum;
  multiselectList: IRoadmapMultiselect[];
  mapId: string = this.route.snapshot.paramMap.get('mapId');

  @Input() selectOptions: IRoadmapParameters;
  @Output() selectionChange = new EventEmitter<any[]>();

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.multiselectList = this.createMultiselectList();
    this.selections = this.multiselectList.map(filter => {
      return {
        selectionType: filter.type,
        selectedOptions: filter.options.map(opt => opt.id)
      };
    });
    this.selectionChange.emit(this.selections);
  }

  selectOption(multiselect: IRoadmapMultiselect, option: IMultiselectOption) {
    const selectedFilter = this.selections.find(({ selectionType }) => selectionType === multiselect.type);

    if (multiselect.isAllSelected) {
      multiselect.isAllSelected = false;
      option.selected = true;
      selectedFilter.selectedOptions = [option.id];

    } else {
      const selectedOptionIndex = selectedFilter.selectedOptions.indexOf(option.id);

      if (selectedOptionIndex === -1) {
        selectedFilter.selectedOptions.push(option.id);
        option.selected = true;

      } else {
        selectedFilter.selectedOptions.splice(selectedOptionIndex, 1);
        option.selected = false;

        if (selectedFilter.selectedOptions.length === 0) {
          multiselect.isAllSelected = true;
          selectedFilter.selectedOptions = multiselect.options.map(opt => opt.id);
        }
      }
    }

    this.selectionChange.emit(this.selections);
  }

  selectAllOptions(multiselect: IRoadmapMultiselect) {
    const selectedFilter = this.selections.find(({ selectionType }) => selectionType === multiselect.type);
    selectedFilter.selectedOptions = multiselect.options.map(opt => opt.id);

    if (multiselect.isAllSelected === false) {
      this.selectionChange.emit(this.selections);
    }
    multiselect.isAllSelected = true;
    multiselect.options.forEach(opt => opt.selected = false);
  }

  private createMultiselectList(): IRoadmapMultiselect[] {

    return Object.keys(this.selectOptions)
      .filter(key => this.selectOptions[key] && (Array.isArray(this.selectOptions[key]) || this.selectOptions[key].id))
      .reduce((acc: IRoadmapMultiselect[], key: SelectionTypeEnum) => {
        const multiselect = { type: key } as IRoadmapMultiselect;

        const setMultiselectProps = (name: string, options: IRoadmapParameter[]) => {
          multiselect.name = name;
          multiselect.expandAllState = 'open';
          multiselect.expandMoreState = 'closed';
          multiselect.isAllSelected = true;
          multiselect.options = options.map<IMultiselectOption>(opt => ({
            id: opt.id,
            name: opt.name,
            selected: false
          }));
        };

        switch (key) {
          case SelectionTypeEnum.Teams:
          case SelectionTypeEnum.Phases: {
            const options: IRoadmapParameter[] = this.selectOptions[key];
            setMultiselectProps(key, options);
            break;
          }

          case SelectionTypeEnum.UserDefined: {
            const filter: IRoadmapCustomParameter = this.selectOptions[key];
            setMultiselectProps(filter.name, filter.options);
            break;
          }
        }

        acc.push(multiselect);
        return acc;
      }, []);
  }
}
