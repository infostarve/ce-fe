import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MultiselectSidebarComponent } from './multiselect-sidebar.component';

@NgModule({
  declarations: [MultiselectSidebarComponent],
  exports: [MultiselectSidebarComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class MultiselectSidebarModule { }
